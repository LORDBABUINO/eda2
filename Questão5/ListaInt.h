#include<No.h>
#include<cstddef>

class ListaInt{

	private:
		No *head = NULL;
		No *tail = NULL;
		No *no = NULL;
	public:
                bool empty(){
                        if (head == NULL and tail == NULL)
                                return true;
                        return false;
                }
		void pushFront(int info){
			no = new No;
			if(empty()){
				head = no;
				tail = no;
			}
			no->setInfo(info);
			no->setNext(head);
			head->setBack(no);
			head = no;
		}
		void popFront(){
			if (empty())
				return;
			if(head == tail){
				delete head;
				head = NULL;
				tail = NULL;
			}
			else{
				
				head = head->getNext();
				delete head->getBack();
				head->setBack(NULL);
			}
		}
		void pushBack(int info){
			no = new No;
			no->setInfo(info);
                        if(empty()){
                                head = no;
                                tail = no;
                        }
			else{
				no->setBack(tail);
				tail->setNext(no);
				tail = no;
			}
		}
		void popBack(){
                        if (empty())
                                return;
                        if(head == tail){
  				delete head;
				head = NULL;
				tail = NULL;
			}
			else{
				tail = tail->getBack();
				delete tail->getNext();
				tail->setNext(NULL);
			}
		}
		int size(){
			if (empty())
				return 0;
			int i;
			for(i=1, no = head; no != tail; i++)
				no = no->getNext();
			return i;
		}
		No *acessarPosicao(int posicao){
			if(empty() || posicao >= size())
				return NULL;
			int i;
			for(i = 0, no = head; i < posicao; i++)
				no = no->getNext();
			return no;
		}
		void mover(int origem,int destino){

			if(origem < 0 or destino < 0 or origem >= size() or destino >= size() or origem == destino)
				return;

			No *trocaNo;
			trocaNo = acessarPosicao(origem);

			if(origem == 0){
				acessarPosicao(origem + 1)->setBack(NULL);
				head = acessarPosicao(origem + 1);
			}
			else if(origem == size() - 1){
				tail = acessarPosicao(origem - 1);
				tail->setNext(NULL);
			}
                        else{
                                acessarPosicao(origem + 1)->setBack(acessarPosicao(origem - 1));
                                acessarPosicao(origem - 1)->setNext(acessarPosicao(origem + 1));
                        }
			if(origem < destino)
				destino--;			

			if(destino == 0){
				trocaNo->setBack(NULL);
				acessarPosicao(destino)->setBack(trocaNo);
				trocaNo->setNext(acessarPosicao(destino));
				head = trocaNo;
			}
			else if(destino == size() - 1){
				trocaNo->setBack(acessarPosicao(destino));
				trocaNo->setNext(NULL);
				acessarPosicao(destino)->setNext(trocaNo);
				tail = trocaNo;
				}
                        else{
				if(origem <= destino){
					trocaNo->setBack(acessarPosicao(destino));
					trocaNo->setNext(acessarPosicao(destino + 1));
					acessarPosicao(destino + 1)->setBack(trocaNo);
					acessarPosicao(destino)->setNext(trocaNo);
				}
				else{
					trocaNo->setBack(acessarPosicao(destino - 1));
					trocaNo->setNext(acessarPosicao(destino));
                               		acessarPosicao(destino)->setBack(trocaNo);
					acessarPosicao(destino - 1)->setNext(trocaNo);
				}
			}
		}
		void swap(int posicao1, int posicao2){
			if(posicao1 == posicao2)
				return;
			if(posicao1 > posicao2){
				int troca;
				troca = posicao1;
				posicao1 = posicao2;
				posicao2 = troca;
			}
			mover(posicao1, posicao2);
			mover(posicao2 - 1, posicao1);
		}
		void esvaziar(){
			while(!empty())
				popBack();
		}
};
