#include <stdio.h>
#include "shellSort.h"

int main () {
	
	int vetor[11] = {7, 6, 9, 10, 1, 2, 4, 5, 3, 8, 0};
	int i = 0;

	puts("Vetor desordenado: ");
	while (i < 11) {
		printf("%d ", vetor[i]);
		i++;
	}
	putchar('\n');
	shellSort (vetor, 11);

	return 0;
}