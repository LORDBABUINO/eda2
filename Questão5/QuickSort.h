#include<ListaInt.h>

ListaInt QuickSort(ListaInt lista, int inicio, int termino){

	if(termino > inicio){
		int i = inicio;
		int j = termino;
		int pivo = lista.acessarPosicao(i)->getInfo();
		while(i <= j){
			for(;lista.acessarPosicao(i)->getInfo() < pivo;i++);
			for(;pivo < lista.acessarPosicao(j)->getInfo();j--);
			if(i <= j){
				lista.swap(i,j);
				i++;
				j--;
			}
		}
		lista = QuickSort(lista,inicio,j);
		lista = QuickSort(lista,i,termino);
	}
	return lista;
}

	
	
